package com.automationanywhere.botcommand.demo;

import Utils.AAEUtils;
import Utils.ControlRoomServer;
import archives.GetBotList;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.core.security.SecureString;

import java.util.HashMap;
import java.util.Map;

public class TestGetBotList {

    public static void main(String[] args) throws Exception {

      //  GetBotList command = new GetBotList();
       GetBotList command = new GetBotList();

        String Login = "iqbot1";
        char[] chLogin = new char[Login.length()];
        for (int i = 0; i < Login.length(); i++) { chLogin[i] = Login.charAt(i); }

        String Password = "automation123";
        char[] chPassword = new char[Password.length()];
        for (int i = 0; i < Password.length(); i++) { chPassword[i] = Password.charAt(i); }

        ControlRoomServer serv = new ControlRoomServer("http://localhost:80",new SecureString(chLogin),new SecureString(chPassword));
        serv.authenticate();
        Map<String,Object> mockSession = new HashMap<String,Object>();
        mockSession.put("Default",serv);
        command.setSessions(mockSession);
        ListValue<DictionaryValue> res = command.action("Default","private","Test");
        AAEUtils.PrintListOfDictionaries(res);

    }
}
