package com.automationanywhere.botcommand.demo;

import Utils.AAEUtils;
import Utils.ControlRoomServer;
import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.core.security.SecureString;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestsQueueAddItems {

    public static void main(String[] args) throws Exception {

      //  GetUserList command = new GetUserList();
       //GetBotList command = new GetBotList();


        String Login = "iqbot1";
        char[] chLogin = new char[Login.length()];
        for (int i = 0; i < Login.length(); i++) { chLogin[i] = Login.charAt(i); }

        String Password = "automation123";
        char[] chPassword = new char[Password.length()];
        for (int i = 0; i < Password.length(); i++) { chPassword[i] = Password.charAt(i); }

        ControlRoomServer serv = new ControlRoomServer("http://52.72.250.194:80",new SecureString(chLogin),new SecureString(chPassword));
        serv.authenticate();
        //Map<String,Object> mockSession = new HashMap<String,Object>();
        //mockSession.put("Default",serv);
        //command.setSessions(mockSession);

        QueueAddWorkitem command = new QueueAddWorkitem(serv.getURL(),serv.getAuthenticationToken());

        //com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext = command.globalSessionContext;


 //{"sort":[{"field":"name","direction":"asc"}],"filter":{"operator":"substring","value":"Get Membership Number From DB","field":"name"},"fields":[],"page":{"offset":0,"total":1,"totalFilter":1,"length":100}}
        LinkedHashMap<String, Value> AllValues = new LinkedHashMap<String, Value>();
        AllValues.put("firstname",new StringValue("yli"));
        AllValues.put("lastname",new StringValue("z"));
        AllValues.put("dob",new StringValue("via JDK"));
        AllValues.put("membershipnumber",new StringValue(""));

                //firstname,lastname,dob,membershipnumber
        //Bren,Sapience,19000101,
        String ID = command.action("Get Membership Number From DB",AllValues).get().get("itemid").toString();
       System.out.println(ID);

    }
}
