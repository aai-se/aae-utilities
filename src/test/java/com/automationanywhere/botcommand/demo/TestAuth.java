package com.automationanywhere.botcommand.demo;

import archives.A_SessionStart;
import com.automationanywhere.core.security.SecureString;

public class TestAuth {

    public static void main(String[] args) throws Exception {
        A_SessionStart start = new A_SessionStart();

        String Login = "admin";
        char[] chLogin = new char[Login.length()];
        for (int i = 0; i < Login.length(); i++) { chLogin[i] = Login.charAt(i); }

        String Password = "Un1ver$e123";
        char[] chPassword = new char[Password.length()];
        for (int i = 0; i < Password.length(); i++) { chPassword[i] = Password.charAt(i); }

        String WrongPassword = "Un1ver$e123456";
        char[] chWrongPassword = new char[WrongPassword.length()];
        for (int i = 0; i < WrongPassword.length(); i++) { chWrongPassword[i] = WrongPassword.charAt(i); }

        start.start("Default","http://localhost:80",new SecureString(chLogin),new SecureString(chPassword));
        //start.start("DefaultW","http://localhost:80",new SecureString(chLogin),new SecureString(chWrongPassword));

    }
}
