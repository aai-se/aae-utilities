package com.automationanywhere.botcommand.demo;

import Utils.ControlRoomServer;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.core.security.SecureString;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestsQueueCheckItemsNotWorking {

    public static void main(String[] args) throws Exception {

      //  GetUserList command = new GetUserList();
       //GetBotList command = new GetBotList();
        QueueCheckWorkitem command = new QueueCheckWorkitem();

        String Login = "iqbot1";
        char[] chLogin = new char[Login.length()];
        for (int i = 0; i < Login.length(); i++) { chLogin[i] = Login.charAt(i); }

        String Password = "automation123";
        char[] chPassword = new char[Password.length()];
        for (int i = 0; i < Password.length(); i++) { chPassword[i] = Password.charAt(i); }

        ControlRoomServer serv = new ControlRoomServer("http://52.72.250.194:80",new SecureString(chLogin),new SecureString(chPassword));
        serv.authenticate();
        Map<String,Object> mockSession = new HashMap<String,Object>();
        mockSession.put("Default",serv);
        command.setSessions(mockSession);

 //{"sort":[{"field":"name","direction":"asc"}],"filter":{"operator":"substring","value":"Get Membership Number From DB","field":"name"},"fields":[],"page":{"offset":0,"total":1,"totalFilter":1,"length":100}}
        LinkedHashMap<String, Value> AllValues = new LinkedHashMap<String, Value>();
        AllValues.put("itemid",new StringValue("5"));
        AllValues.put("queueid",new StringValue("2"));


                //firstname,lastname,dob,membershipnumber
        //Bren,Sapience,19000101,
        DictionaryValue dictVal = command.action(AllValues);
        Map<String,Value> DictMap = dictVal.get();

        System.out.println("Dict Item:"+DictMap);


    }
}
