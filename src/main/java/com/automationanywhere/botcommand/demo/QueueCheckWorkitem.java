package com.automationanywhere.botcommand.demo;


import Utils.ControlRoomServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.parser.ParseException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Get Work Item Details", name="Get Work Item Details", description="Get Work Item Details", icon="pkg.svg",
        node_label="Get Work Item Details",
        return_type= DICTIONARY, return_sub_type = STRING, return_label="Work Item Coordinates (Dictionary with 4 static keys (WORKITEMVERSION, WORKITEMID, WORKITEMRESULT, WORKITEMSTATUS) and variable keys for all Columns)", return_required=true)

public class QueueCheckWorkitem {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public String CRURL;
    public String AUTHTOKEN;

    public QueueCheckWorkitem(){}

    public QueueCheckWorkitem(String CrUrl, String AuthToken){
        this.CRURL = CrUrl;
        this.AUTHTOKEN = AuthToken;
    }
    @GlobalSessionContext
    private com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }
    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public DictionaryValue action(
            //@Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
             @Idx(index = "1", type = AttributeType.DICTIONARY) @Pkg(label = "Work Item Coordinates (Dictionary with 'itemid' and 'queueid')", default_value_type = DataType.DICTIONARY) @NotEmpty Map<String, Value> WorkItemCoordinates
    )
    {


        if(this.CRURL == null){
            this.CRURL = ((com.automationanywhere.bot.service.GlobalSessionContext) this.globalSessionContext).getCrUrl();
        }

        if(this.AUTHTOKEN == null){
            this.AUTHTOKEN = ((com.automationanywhere.bot.service.GlobalSessionContext) this.globalSessionContext).getUserToken();
        }




        ControlRoomServer serv = new ControlRoomServer(this.CRURL,this.AUTHTOKEN);

        RestRequests reqs = new RestRequests(serv);

        String ITEMIDKEY = "itemid";
        String QUEUEIDKEY = "queueid";

        if (!WorkItemCoordinates.containsKey(ITEMIDKEY)) { throw new BotCommandException(MESSAGES.getString("missingDictKey", ITEMIDKEY)); }
        if (!WorkItemCoordinates.containsKey(QUEUEIDKEY)) { throw new BotCommandException(MESSAGES.getString("missingDictKey", QUEUEIDKEY)); }

        String WorkitemID = WorkItemCoordinates.get(ITEMIDKEY).toString();
        String QueueID = WorkItemCoordinates.get(QUEUEIDKEY).toString();

        if (WorkitemID.isEmpty()) { throw new BotCommandException(MESSAGES.getString("emptyDictValue", ITEMIDKEY)); }
        if (QueueID.isEmpty()) { throw new BotCommandException(MESSAGES.getString("emptyDictValue", QUEUEIDKEY)); }

        Map<String, Value> WorkitemInfo = new HashMap<String,Value>();
        try {
            WorkitemInfo = reqs.GetWorkItemInfo(QueueID,WorkitemID);
        } catch (UnirestException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(WorkitemInfo.isEmpty()){ throw new BotCommandException(MESSAGES.getString("missingWorkitemInfo", QueueID+":"+WorkitemID)); }
        DictionaryValue DictRet = new DictionaryValue();
        DictRet.set(WorkitemInfo);
        return DictRet;

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }


}
