package com.automationanywhere.botcommand.demo;


import Utils.*;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="Add Work Item", name="Add Work Item", description="Add Work Item to a Queue", icon="pkg.svg",
        node_label="Add Work Item",
        return_type= DataType.DICTIONARY, return_sub_type = DataType.STRING, return_label="Work Item Coordinates (Dictionary with 2 keys: 'itemid' and 'queueid')", return_required=true)

public class QueueAddWorkitem {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");
    public String CRURL;
    public String AUTHTOKEN;

    public QueueAddWorkitem(){}

    public QueueAddWorkitem(String CrUrl, String AuthToken){
        this.CRURL = CrUrl;
        this.AUTHTOKEN = AuthToken;
    }

    @GlobalSessionContext
    public com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public DictionaryValue action(
           // @Idx(index = "1", type = TEXT) @Pkg(label = "Session Name", default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "1", type = TEXT) @Pkg(label = "Queue Name", default_value_type = STRING, default_value = "") @NotEmpty String QueueName,
            @Idx(index = "2", type = AttributeType.DICTIONARY) @Pkg(label = "Dictionary containing all Columns and Values for Work Item", default_value_type = DataType.DICTIONARY)  @NotEmpty Map<String, Value> WorkItemValues
    )
    {

        if(WorkItemValues==null){
            throw new BotCommandException(MESSAGES.getString("inputWIDictNull"));
        }

        if(this.CRURL == null){
            this.CRURL = ((com.automationanywhere.bot.service.GlobalSessionContext) this.globalSessionContext).getCrUrl();
        }

        if(this.AUTHTOKEN == null){
            this.AUTHTOKEN = ((com.automationanywhere.bot.service.GlobalSessionContext) this.globalSessionContext).getUserToken();
        }


        ControlRoomServer serv = new ControlRoomServer(this.CRURL,this.AUTHTOKEN);
        RestRequests reqs = new RestRequests(serv);

        //String crURL = this.sessionContext.;
        //String token = this.globalSessionContext.getUserToken();

        if(WorkItemValues.isEmpty()){
            throw new BotCommandException(MESSAGES.getString("inputWIDictError"));
        }

        // Preparing the User IDs as Array as per REST API Format
        String QueueID = "";
        try {
            QueueID = reqs.GetQueueIDFromName(QueueName);
            //System.out.println("DEBUG Queue ID:"+QueueID);
        } catch (UnirestException e) {
            throw new BotCommandException(MESSAGES.getString("serverSideError", e.getMessage()));
        }

        String WIID = "";
        try {
            WIID = reqs.AddWorkitemToQueue(QueueID,WorkItemValues);
        } catch (UnirestException e) {
            throw new BotCommandException(MESSAGES.getString("serverSideError", e.getMessage()));
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("serverSideError", e.getMessage()));
        }

        System.out.println("DEBUG Work Item ID:"+WIID);
        LinkedHashMap<String,Value> ReturnValues = new LinkedHashMap<String,Value>();
        ReturnValues.put("itemid",new StringValue(WIID));
        ReturnValues.put("queueid",new StringValue(QueueID));

        DictionaryValue DictRet = new DictionaryValue();
        DictRet.set(ReturnValues);
        return DictRet;

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
