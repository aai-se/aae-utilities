package archives;

import Utils.AAEUtils;
import Utils.ControlRoomServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.BooleanValue;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//@BotCommand
@CommandPkg(label="Get Folder List", name="Get Folder List", description="Get Folder List", icon="pkg.svg",
        node_label="Get Folder List",
        return_type= DataType.LIST, return_sub_type = DataType.ANY, return_label="list of dictionaries with 4 keys: 'id', 'parentid', 'name' & 'path'", return_required=true)


public class GetFolderList {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2",  type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "2.1", pkg = @Pkg(label = "List Private Folders", value = "private")),
                    @Idx.Option(index = "2.2", pkg = @Pkg(label = "List Public Folders", value = "public")),
            }) @Pkg(label = "Pick an Option", default_value = "private", default_value_type = DataType.STRING) @NotEmpty String TypeOfFoldersToList
    ) {

        ControlRoomServer serv = (ControlRoomServer) this.sessions.get(sessionName);
        ListValue<DictionaryValue> LisOfObjects = new ListValue<DictionaryValue>();
        RestRequests reqs = new RestRequests(serv);
        Boolean ListPublicFolder = false;
        if (TypeOfFoldersToList.equalsIgnoreCase("public")) {
            ListPublicFolder = true;
        }
        try {
            ArrayList<HashMap<String, Value>> AllFoldersInfo = reqs.GetFolderList(ListPublicFolder);
            LisOfObjects= AAEUtils.ConvertHashmapListToListValue(AllFoldersInfo);
            return LisOfObjects;

        } catch (UnirestException e) {
            return LisOfObjects;
        }
    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
