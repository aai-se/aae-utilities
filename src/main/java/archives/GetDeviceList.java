package archives;

import Utils.AAEUtils;
import Utils.ControlRoomServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//@BotCommand
@CommandPkg(label="Get Device List", name="Get Device List", description="Get Device List", icon="pkg.svg",
        node_label="Get Device List",
        return_type= DataType.LIST, return_sub_type = DataType.ANY, return_label="list of dictionaries with 8 keys: 'id', 'type', 'hostname', 'userid', 'username', 'status', 'poolname' & 'botagentversion'", return_required=true)
/*
                           FolderInfoHash.put("id",new StringValue(ObjectID));
                FolderInfoHash.put("type",new StringValue(ObjectType));
                FolderInfoHash.put("hostname",new StringValue(ObjectHostName));
                FolderInfoHash.put("userid",new StringValue(ObjectUserId));
                FolderInfoHash.put("username",new StringValue(ObjectUserName));
                FolderInfoHash.put("status",new StringValue(ObjectStatus));
                FolderInfoHash.put("poolname",new StringValue(ObjectPoolName));
                FolderInfoHash.put("botagentversion",new StringValue(ObjectBotAgentVersion));
*/
public class GetDeviceList {


    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName
    ) {

        ControlRoomServer serv = (ControlRoomServer) this.sessions.get(sessionName);
        ListValue<DictionaryValue> LisOfObjects = new ListValue<DictionaryValue>();
        RestRequests reqs = new RestRequests(serv);

        try {
            ArrayList<HashMap<String, Value>> AllFoldersInfo = reqs.GetDeviceList();
            LisOfObjects= AAEUtils.ConvertHashmapListToListValue(AllFoldersInfo);
            return LisOfObjects;

        } catch (UnirestException e) {
            return LisOfObjects;
        }
    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
