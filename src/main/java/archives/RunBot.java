package archives;


import Utils.*;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.net.httpserver.HttpServer;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import static com.automationanywhere.commandsdk.model.AttributeType.NUMBER;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.DICTIONARY;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//@BotCommand
@CommandPkg(label="Run Remote Bot", name="Run Remote Bot", description="Takes in the Bot ID and Bot Name to trigger a bot", icon="pkg.svg",
        node_label="Remote Bot",
        return_type= DataType.LIST, return_sub_type = DataType.ANY, return_label="list of dictionaries with 3 keys: 'page', 'score' & 'class'", return_required=true)

public class RunBot {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
            @Idx(index = "2", type = TEXT) @Pkg(label = "Bot Name", default_value_type = STRING, default_value = "") @NotEmpty String BotName,
            @Idx(index = "3", type = TEXT) @Pkg(label = "Device Pool Name", default_value_type = STRING, default_value = "") @NotEmpty String DevicePoolName,
            @Idx(index = "4", type = TEXT) @Pkg(label = "Username (Run As)", default_value_type = STRING, default_value = "") @NotEmpty String RunAsName,
            @Idx(index = "5", type = AttributeType.CHECKBOX) @Pkg(label = "Wait for Bot Output", default_value_type = DataType.BOOLEAN, default_value = "false")  Boolean WaitForBotOutput,
                @Idx(index = "5.1", type = AttributeType.NUMBER) @Pkg(label = "Port for Callback from CR", default_value_type = DataType.NUMBER, default_value="8200", description="ex: 8200") @NotEmpty Number CallbackPort,
                @Idx(index = "5.2", type = NUMBER) @Pkg(label = "Timeout (in seconds)", default_value_type = DataType.NUMBER, default_value = "60",description = "ex: 60") @NotEmpty Number TimeOutPeriod,
            @Idx(index = "6", type = AttributeType.CHECKBOX) @Pkg(label = "Pass Input Parameters to Bot", default_value_type = DataType.BOOLEAN, default_value = "false")  Boolean PassInputParamsToBot,
             @Idx(index = "6.1", type = AttributeType.LIST) @Pkg(label = "List of Dictionaries", default_value_type = DataType.LIST)  LinkedHashMap<String, Value> InputParameters,
            @Idx(index = "7", type = AttributeType.CHECKBOX) @Pkg(label = "Use Public IP for Callback", default_value_type = DataType.BOOLEAN, default_value = "false")  Boolean UsePublicIP
    )
    {

        ListValue ResultsList = new ListValue();
        /*
            Results returned by the action is a List of Dictionaries. first dictionary contains the following keys: 'deploymentid','deviceid','userid' and 'status'
            All other dictionaries represent an Output Variable retrieved from the Bot, each with 2 keys:
            - 'type': STRING, BOOLEAN, NUMBER, LIST, DICTIONARY
            - 'value': the actual value of the output
         */

        ControlRoomServer serv = (ControlRoomServer) this.sessions.get(sessionName);
        RestRequests reqs = new RestRequests(serv);

        // Preparing the User IDs as Array as per REST API Format
        String BotID = "";
        try {
            BotID = reqs.GetBotIdFromName(true,BotName);
            System.out.println("DEBUG Bot ID:"+BotID);
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        ArrayList<String> AllUserIDs = new ArrayList<String>();
        String UserID ="";
        try {
            UserID = reqs.GetUseIdFromName(RunAsName);
            System.out.println("DEBUG User ID:"+UserID);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        AllUserIDs.add(UserID);

        // Preparing the Pool IDs as Array as per REST API Format
        ArrayList<String> AllPoolIDs = new ArrayList<String>();
        String DevicePoolID ="";
        try {
            DevicePoolID = reqs.GetDevicePoolIdFromName(DevicePoolName);
            System.out.println("DEBUG Device Pool ID:"+DevicePoolID);
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        AllPoolIDs.add(DevicePoolID);

        // Preparing the callback URL.
        // !! This can be easily improved by generating a new GUID as the Endpoint instead of a Hardcoded value
        String CallBackURLComplete = null;
        String IP = "";
        UUID uuid = UUID.randomUUID();
        String Endpoint = "/"+uuid.toString().replaceAll("-","");

        if(WaitForBotOutput){
            if(UsePublicIP){
                IP = AAEUtils.GetPublicIP();
            }else{
                IP = AAEUtils.GetPrivateIP();
            }
            System.out.println("IP:"+IP);
            //IP = "52.72.250.194";
            CallBackURLComplete = "http://"+IP+":" + CallbackPort+Endpoint; // this should be the public IP of the current server.. maybe get it dynamically?
            System.out.println("DEBUG CallBack:"+CallBackURLComplete);
        }

        try {
            // UniqueID should be changed to a GUID, BotName should be retrieved dynamically
            String UniqueID = uuid.toString()+"-RemoteBot";
            String JSONBODY = JsonUtils.GetRunBotBody(UniqueID,BotID,BotName,AllUserIDs,AllPoolIDs,CallBackURLComplete, null);
            System.out.println("DEBUG - JSON Body Generated for Run Request:"+JSONBODY);

            String deploymentId = reqs.RunBot(JSONBODY);
            System.out.println("DEBUG - Deployment ID"+deploymentId);

            if(WaitForBotOutput){
                // We initiate a custom web server and wait for the output..
                CallbackNetServer CallbackServer = new CallbackNetServer(CallbackPort.intValue(),Endpoint,TimeOutPeriod.intValue(),deploymentId);

                // we then process the output retrieved from the CR
                String JSONRESPONSE = CallbackServer.runServer();
                JSONParser parser = new JSONParser();
                try{
                    parser.parse(JSONRESPONSE);
                }catch(ParseException e){}

                // The output From CR needs to be converted into a List of Dictionaries (see above for details)
                ArrayList<DictionaryValue> AllRes = RestResponse.ProcessDeplopymentResponse(JSONRESPONSE);
                ResultsList = new ListValue();
                ResultsList.set(AllRes);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResultsList;

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }


}
