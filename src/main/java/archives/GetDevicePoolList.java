package archives;

import Utils.AAEUtils;
import Utils.ControlRoomServer;
import Utils.RestRequests;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.DataType;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

//@BotCommand
@CommandPkg(label="Get Device Pool List", name="Get Device Pool List", description="Get Device Pool List", icon="pkg.svg",
        node_label="Get Device Pool List",
        return_type= DataType.LIST, return_sub_type = DataType.ANY, return_label="list of dictionaries with 4 keys: 'id', 'parentid', 'name' & 'path'", return_required=true)


public class GetDevicePoolList {


    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public ListValue<DictionaryValue> action(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,  default_value = "Default") @NotEmpty String sessionName
    ) {

        ControlRoomServer serv = (ControlRoomServer) this.sessions.get(sessionName);
        ListValue<DictionaryValue> LisOfObjects = new ListValue<DictionaryValue>();
        RestRequests reqs = new RestRequests(serv);

        try {
            ArrayList<HashMap<String, Value>> AllObjectsInfo = reqs.GetDevicePoolList("");
            LisOfObjects= AAEUtils.ConvertHashmapListToListValue(AllObjectsInfo);
            return LisOfObjects;

        } catch (UnirestException e) {
            return LisOfObjects;
        }
    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}
