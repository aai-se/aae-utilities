package archives;

import Utils.ControlRoomServer;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.core.security.SecureString;

import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.CREDENTIAL;
import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.RECORD;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */


//@BotCommand
@CommandPkg(label = "Session Start", name = "SessionStart", description = "Session Start", icon = "", node_label = "session start {{sessionName}}|")
public class A_SessionStart {

    //private static final Logger logger = LogManager.getLogger(StartSession.class);

    @Sessions
    private Map<String, Object> sessions = new HashMap<String, Object>();

    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.demo.messages");

    @GlobalSessionContext
    public com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext;

    public void setGlobalSessionContext(com.automationanywhere.bot.service.GlobalSessionContext globalSessionContext) {
        this.globalSessionContext = globalSessionContext;
    }

    @Execute
    public void start(@Idx(index = "1", type = TEXT) @Pkg(label = "Session Name",  default_value_type = STRING, default_value = "Default") @NotEmpty String sessionName,
                      @Idx(index = "2", type = TEXT) @Pkg(label = "Control Room Base URL",  default_value_type = STRING, default_value = "http://localhost") @NotEmpty String CRURL,
                      @Idx(index = "3", type = CREDENTIAL) @Pkg(label = "Control Room Login",  default_value_type = RECORD) @NotEmpty SecureString CRLogin,
                      @Idx(index = "4", type = CREDENTIAL) @Pkg(label = "Control Room Password",  default_value_type = RECORD) @NotEmpty SecureString CRPwd
    ) throws Exception {

        // Check for existing session
        if (this.sessions.containsKey(sessionName)){
            throw new BotCommandException(MESSAGES.getString("sessionInUse")) ;
        }

        ControlRoomServer myBackendServ = new ControlRoomServer(CRURL,CRLogin,CRPwd);

        Boolean AuthResp  = myBackendServ.authenticate();
        //System.out.println("Auth: "+AuthResp);

        if(!AuthResp){
            throw new BotCommandException(MESSAGES.getString("authFail")) ;
        }

        this.sessions.put(sessionName, myBackendServ);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }

}
