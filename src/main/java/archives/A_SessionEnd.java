package archives;

import Utils.ControlRoomServer;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;

import java.util.Map;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;
//import java.net.http.HttpResponse;

/**
 * @author Brendan Sapience
 */

//@BotCommand
@CommandPkg(label = "Session End", name = "SessionEnd", description = "Session End", icon = "", node_label = "session end {{sessionName}}")
public class A_SessionEnd {

    @Sessions
    private Map<String, Object> sessions;

    @Execute
    public void end(
            @Idx(index = "1", type = TEXT) @Pkg(label = "Session name", default_value_type = STRING,
                    default_value = "Default") @NotEmpty String sessionName) {

        ControlRoomServer serv  = (ControlRoomServer) this.sessions.get(sessionName);

        sessions.remove(sessionName);

    }

    public void setSessions(Map<String, Object> sessions) {
        this.sessions = sessions;
    }
}