package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;

public class RestResponse{

    public static ArrayList<DictionaryValue> ProcessDeplopymentResponse(String JsonResponse){

        JSONParser parser = new JSONParser();
        ArrayList<DictionaryValue> ResponseList = new ArrayList<DictionaryValue>();
        try{
            JSONObject jobj = (JSONObject) parser.parse(JsonResponse);

            // The first Dict we are returning contains info on the status of the Bot Run
            String DeviceID = (String) jobj.get("deviceId");
            String UserID =  (String) jobj.get("userId");
            String DeploymentID =  (String) jobj.get("deploymentId");
            String Status =  (String) jobj.get("status");

            HashMap<String, Value> InfoDict = new HashMap<String,Value>();
            InfoDict.put("deviceid",new StringValue(DeviceID));
            InfoDict.put("userid",new StringValue(UserID));
            InfoDict.put("deploymentid",new StringValue(DeploymentID));
            InfoDict.put("status",new StringValue(Status));

            DictionaryValue InfoDictVal = new DictionaryValue();
            InfoDictVal.set(InfoDict);

            ResponseList.add(InfoDictVal);

            JSONObject BotOutput = (JSONObject) jobj.get("botOutput");

            // All other Dictionaries we are returning each represent an output variable returned by the bot
            BotOutput.keySet().forEach(key -> {
                HashMap<String, Value> VarDict = new HashMap<String,Value>();

                JSONObject OutputObject = (JSONObject) BotOutput.get(key);

                String OutputObjectType = (String) OutputObject.get("type");

                VarDict.put("type",new StringValue(OutputObjectType));
                String OutputObjectValue = "";

                if(OutputObjectType.equals("NUMBER")){ OutputObjectValue = (String) OutputObject.get("number"); }
                if(OutputObjectType.equals("BOOLEAN")){OutputObjectValue = (String) OutputObject.get("boolean");}
                if(OutputObjectType.equals("STRING")){OutputObjectValue = (String) OutputObject.get("string");}
                VarDict.put("value",new StringValue(OutputObjectValue));
                DictionaryValue ValueDictVal = new DictionaryValue();
                ValueDictVal.set(VarDict);
                ResponseList.add(ValueDictVal);
            });

        }catch(ParseException e){}
        return ResponseList;
    }

    /*
    {
  "deviceId": "20",
  "userId": "95",
  "deploymentId": "f9c2e990-1d3f-4f93-b3fa-01f0120b9a45",
  "status": "RUN_COMPLETED",
  "botOutput": {
    "iOutputNumber": {
      "type": "NUMBER",
      "expression": "",
      "string": "",
      "number": "0.0",
      "boolean": "",
      "repository": "",
      "variableName": "",
      "packageId": "",
      "packageName": "",
      "iteratorId": "",
      "iteratorName": "",
      "conditionalId": "",
      "conditionalName": "",
      "list": [],
      "dictionary": [],
      "exceptionName": "",
      "thumbnailMetadataPath": "",
      "screenshotMetadataPath": "",
      "properties": [],
      "triggerName": "",
      "formElementId": "",
      "formElementType": "",
      "formInstanceId": ""
    },
    "bOutputBoolean": {
      "type": "BOOLEAN",
      "expression": "",
      "string": "",
      "number": "",
      "boolean": "false",
      "repository": "",
      "variableName": "",
      "packageId": "",
      "packageName": "",
      "iteratorId": "",
      "iteratorName": "",
      "conditionalId": "",
      "conditionalName": "",
      "list": [],
      "dictionary": [],
      "exceptionName": "",
      "thumbnailMetadataPath": "",
      "screenshotMetadataPath": "",
      "properties": [],
      "triggerName": "",
      "formElementId": "",
      "formElementType": "",
      "formInstanceId": ""
    },
    "sOutputString1": {
      "type": "STRING",
      "expression": "",
      "string": "",
      "number": "",
      "boolean": "",
      "repository": "",
      "variableName": "",
      "packageId": "",
      "packageName": "",
      "iteratorId": "",
      "iteratorName": "",
      "conditionalId": "",
      "conditionalName": "",
      "list": [],
      "dictionary": [],
      "exceptionName": "",
      "thumbnailMetadataPath": "",
      "screenshotMetadataPath": "",
      "properties": [],
      "triggerName": "",
      "formElementId": "",
      "formElementType": "",
      "formInstanceId": ""
    },
    "dOutputDict1": {
      "type": "DICTIONARY",
      "expression": "",
      "string": "",
      "number": "",
      "boolean": "",
      "repository": "",
      "variableName": "",
      "packageId": "",
      "packageName": "",
      "iteratorId": "",
      "iteratorName": "",
      "conditionalId": "",
      "conditionalName": "",
      "list": [],
      "dictionary": [{
        "key": "Key A",
        "value": {
          "type": "STRING",
          "expression": "",
          "string": "a value a",
          "number": "",
          "boolean": "",
          "repository": "",
          "variableName": "",
          "packageId": "",
          "packageName": "",
          "iteratorId": "",
          "iteratorName": "",
          "conditionalId": "",
          "conditionalName": "",
          "list": [],
          "dictionary": [],
          "exceptionName": "",
          "thumbnailMetadataPath": "",
          "screenshotMetadataPath": "",
          "properties": [],
          "triggerName": "",
          "formElementId": "",
          "formElementType": "",
          "formInstanceId": ""
        }
      }, {
        "key": "Key B",
        "value": {
          "type": "STRING",
          "expression": "",
          "string": "b value b",
          "number": "",
          "boolean": "",
          "repository": "",
          "variableName": "",
          "packageId": "",
          "packageName": "",
          "iteratorId": "",
          "iteratorName": "",
          "conditionalId": "",
          "conditionalName": "",
          "list": [],
          "dictionary": [],
          "exceptionName": "",
          "thumbnailMetadataPath": "",
          "screenshotMetadataPath": "",
          "properties": [],
          "triggerName": "",
          "formElementId": "",
          "formElementType": "",
          "formInstanceId": ""
        }
      }],
      "exceptionName": "",
      "thumbnailMetadataPath": "",
      "screenshotMetadataPath": "",
      "properties": [],
      "triggerName": "",
      "formElementId": "",
      "formElementType": "",
      "formInstanceId": ""
    }
  }
}
     */

}