package Utils;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CallbackNetServer {

    private String CallBackPath="";
    private int PortNumber = -1;
    private int TimeoutInSeconds = 60;
    private String DeploymentID = "";
    private String JSONRESPONSE = "";

    public CallbackNetServer(int Port, String CallbackPath, int TimeoutInSeconds, String DeploymentID){
        this.CallBackPath = CallbackPath;
        this.PortNumber=Port;
        this.TimeoutInSeconds = TimeoutInSeconds;
        this.DeploymentID = DeploymentID;
    }

    public String runServer() throws Exception {
        final ExecutorService ex = Executors.newSingleThreadExecutor();
        ex.awaitTermination(this.TimeoutInSeconds, TimeUnit.SECONDS); // this ensures the server shuts down after timeout and does not block the action in case it fails in CR
        final CountDownLatch c = new CountDownLatch(1);
        HttpServer server = HttpServer.create(new InetSocketAddress(this.PortNumber), 0);
        server.createContext(this.CallBackPath, (HttpExchange httpExchange) ->
        {
            // Getting responses sent to this new server instance and converting it into a JSON Object
            InputStreamReader isr =  new InputStreamReader(httpExchange.getRequestBody(),"utf-8");
            BufferedReader br = new BufferedReader(isr);
            int b;
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }
            br.close();
            isr.close();
            String CRResponse = buf.toString();
            System.out.println("DEBUG Response Received: "+CRResponse);
            JSONParser parser = new JSONParser();
            try{
                // Getting the Deployment ID from the Response. If the Deployment ID is not the same, then we need to wait. Maybe some other thing called our server?
                JSONObject jobj = (JSONObject) parser.parse(CRResponse);
                String DeploymentIDFromCR = (String) jobj.get("deploymentId");
                //System.out.println("DEBUG: "+DeploymentIDFromCR+ " vs.: "+ DeploymentID);
                if(DeploymentIDFromCR.equalsIgnoreCase(DeploymentID)){
                    // The deployment IDs match.. so we are receiving the response we needed from the same Bot Run!
                    this.JSONRESPONSE = CRResponse;
                    c.countDown();
                }// Else? we wait..
            }catch(ParseException e){
                // should do something.. maybe?
            }
        });

        server.setExecutor(ex);      // set up a custom executor for the server
        server.start();              // start the server
        //System.out.println("DEBUG: HTTP server started");
        c.await();                   // wait until `c.countDown()` is invoked
        ex.shutdown();               // send shutdown command to executor
        // wait until all tasks complete (i. e. all responses are sent)
        ex.awaitTermination(this.TimeoutInSeconds, TimeUnit.SECONDS); // this ensures the server shuts down after timeout and does not block the action in case it fails in CR
        server.stop(0);

        //System.out.println("DEBUG: HTTP server stopped");
        return this.JSONRESPONSE;
    }


}
