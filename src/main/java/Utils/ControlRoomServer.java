package Utils;

import com.automationanywhere.core.security.SecureString;
import com.mashape.unirest.http.exceptions.UnirestException;

public class ControlRoomServer {
    public String getURL() {
        return URL;
    }

    private String URL;

    public SecureString getLogin() {
        return Login;
    }

    public SecureString getPwd() {
        return Pwd;
    }

    public String getAuthenticationToken() {
        return AuthenticationToken;
    }

    private SecureString Login;
    private SecureString Pwd;

    public void setAuthenticationToken(String authenticationToken) {
        AuthenticationToken = authenticationToken;
    }

    private String AuthenticationToken;

    public ControlRoomServer(String BackendURL, SecureString login, SecureString Pwd){

        this.URL = BackendURL;
        this.Login = login;
        this.Pwd = Pwd;

    }

    public ControlRoomServer(String BackendURL, String AuthToken){

        this.URL = BackendURL;

        this.AuthenticationToken = AuthToken;

    }

    public Boolean authenticate() throws UnirestException {
        // Should authenticate and get / save token
        RestRequests utils = new RestRequests(this);
        return utils.AuthenticateToControlRoom();
        //return true;
    }

    public Boolean isAuthActive(){
        return true;
    }

    public Boolean renewAuthentication(){
        return true;
    }
}
