package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AAEUtils {

    public static ListValue<DictionaryValue> ConvertHashmapListToListValue(ArrayList<HashMap<String, Value>> AllObjects){
        ListValue ReturnList = new ListValue();
        ArrayList<Value> TempListHolder = new ArrayList<Value>();
        for(int i=0;i<AllObjects.size();i++){
            DictionaryValue FolderInfo = new DictionaryValue();
            FolderInfo.set(AllObjects.get(i));
            TempListHolder.add(FolderInfo);
        }
        ReturnList.set(TempListHolder);
        return ReturnList;

    }

    public static String GetPrivateIP(){
        String IP = "";
        try(final DatagramSocket socket = new DatagramSocket()){
            socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
            IP = socket.getLocalAddress().getHostAddress();
        } catch (UnknownHostException | SocketException e) {
            e.printStackTrace();
        }
        return IP;
    }

    public static String GetPublicIP() {
        String IP = "";
        try{
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            IP = in.readLine(); //you get the IP as a String
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return IP;
    }



    public static void PrintListOfDictionaries(ListValue<DictionaryValue> Res){
        List<Value> AllValues = Res.get();
        for(int i=0;i<AllValues.size();i++){
            DictionaryValue dict = (DictionaryValue) AllValues.get(i);
            Map<String,Value> DictMap = dict.get();

            System.out.println("Dict Item:"+DictMap);
        }
    }

    public static String ConvertScoreToHumanReadable(double RawConfidenceScore){
        RawConfidenceScore = RawConfidenceScore * 100;
        double RoundedScore = round(RawConfidenceScore, 2);
        return Double.toString(RoundedScore);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
