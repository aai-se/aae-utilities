package Utils;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

public class RestRequests {
    private ControlRoomServer CRServer = null;
    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    public RestRequests(ControlRoomServer myCRServer) {

        this.CRServer = myCRServer;
    }


    public HashMap<String,Value> GetWorkItemInfo(String QueueID, String WorkitemID) throws UnirestException, ParseException {
        String URL = this.CRServer.getURL() + "/v3/wlm/queues/"+QueueID+"/workitems/"+WorkitemID;
        String AuthToken = this.CRServer.getAuthenticationToken();
        System.out.println("Token:" + AuthToken);
        System.out.println(URL);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .asString();

        String JsonResponse = response.getBody();
        System.out.println("DEBUG RESPONSE:" + JsonResponse);
        JSONParser parser = new JSONParser();

        HashMap<String, Value> ResMap = new HashMap<String, Value>();
        JSONObject jobj = (JSONObject) parser.parse(JsonResponse);
        String WorkItemID = (String) jobj.get("id");
        String Version = (String) jobj.get("version");
        String WorkItemStatus = (String) jobj.get("status");
        String WorkItemResult = (String) jobj.get("result");
        ResMap.put("WORKITEMVERSION",new StringValue(Version));
        ResMap.put("WORKITEMID",new StringValue(WorkItemID));
        ResMap.put("WORKITEMSTATUS",new StringValue(WorkItemStatus));
        ResMap.put("WORKITEMRESULT",new StringValue(WorkItemResult));
        JSONObject WorkItemContent = (JSONObject) jobj.get("json");

        if(jobj.containsKey("message")){
            String ErrorMesg = (String) jobj.get("message");
            //ErrorMesg.replaceAll("in json","");
            throw new BotCommandException(MESSAGES.getString("backEndError",ErrorMesg.replaceAll("in json","") ));
        }

        for(Iterator iterator = WorkItemContent.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();
            String Value = (String) WorkItemContent.get(key);
            ResMap.put(key,new StringValue(Value));
        }

        return ResMap;
    }

    public String AddWorkitemToQueue(String QueueID, Map<String, Value> WorkItemValues) throws UnirestException, ParseException {
        String URL = this.CRServer.getURL() + "/v3/wlm/queues/"+QueueID+"/workitems";
        String AuthToken = this.CRServer.getAuthenticationToken();
        //System.out.println("Token:" + AuthToken);
        //System.out.println(URL);
        String JSONBODY =JsonUtils.GetAddWorkItemBody(WorkItemValues);
        //System.out.println(JSONBODY);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body(JSONBODY)
                .asString();
        String JsonResponse = response.getBody();
        JSONParser parser = new JSONParser();

        System.out.println("DEBUG RESPONSE:" + JsonResponse);
        JSONObject jobj = (JSONObject) parser.parse(JsonResponse);

        if(jobj.containsKey("message")){
            String ErrorMesg = (String) jobj.get("message");
            //ErrorMesg.replaceAll("in json","");
            throw new BotCommandException(MESSAGES.getString("missingWIInputValues",ErrorMesg.replaceAll("in json","") ));
        }
    //{"message":"Required column 'lastname' missing in json"}



        JSONArray ListOfWorkitems = (JSONArray) jobj.get("list");
        if(ListOfWorkitems.size()==0){
            throw new BotCommandException(MESSAGES.getString("noWorkItem",QueueID ));
        }
        JSONObject FirstWI = (JSONObject) ListOfWorkitems.get(0);
        return FirstWI.get("id").toString();
    }

    public String GetQueueIDFromName(String QueueName) throws UnirestException {
        if(GetQueueListByName(QueueName).size()==0){
            throw new BotCommandException(MESSAGES.getString("queueNotFound", QueueName));
        }
        return GetQueueListByName(QueueName).get(0).get("id").get().toString();
    }

    public ArrayList<HashMap<String,Value>>  GetQueueListByName(String ObjectName) throws UnirestException {
        String URL = this.CRServer.getURL() + "/v3/wlm/queues/list";
        String AuthToken = this.CRServer.getAuthenticationToken();
        String BODY = JsonUtils.GetSearchBody(ObjectName,"name");

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body(BODY)
                .asString();
        String JsonResponse = response.getBody();
        System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        ArrayList<HashMap<String,Value>> AllObjectInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray ObjectList = (JSONArray) jobj.get("list");
            for(int j=0;j<ObjectList.size();j++){
                HashMap<String,Value> ObjectInfo = new HashMap<String,Value>();
                JSONObject anObject = (JSONObject) ObjectList.get(j);
                String ObjectID = anObject.get("id").toString();
                String ObjectNameFromJson = anObject.get("name").toString();
                ObjectInfo.put("id",new StringValue(ObjectID));
                ObjectInfo.put("name",new StringValue(ObjectNameFromJson));
                AllObjectInfo.add(ObjectInfo);
            }
            return AllObjectInfo;
        } catch (ParseException e) {
            throw new BotCommandException(MESSAGES.getString("queueNotFound", ObjectName));
        }
    }

    public ArrayList<HashMap<String,Value>> GetFolderList(Boolean PublicList) throws UnirestException {

      /*
      "id": "10",
      "parentId": "9",
      "name": "Bots",
      "path": "Automation Anywhere\\Bots",
      "description": "",
      "type": "application/vnd.aa.directory",
      "size": "0",
      "folder": true,
      "folderCount": "10",
      "productionVersion": "",
      "latestVersion": "",
      "locked": false,
      "lockedBy": "0",
      "createdBy": "3",
      "lastModifiedBy": "3",
      "lastModified": "2019-11-19T19:25:35.909Z",
      "workspaceId": "3",
      "botStatus": "DRAFT",
      "hasErrors": false,
      "workspaceType": "UNKNOWN",
      "metadata": false,
      "uri": "",
      "version": "0",
      "hasTriggers": false
      */

        String EndpointForPublicFolder = "/v2/repository/workspaces/public/folders";
        String EndpointForPrivateFolder = "/v2/repository/workspaces/private/folders";

        String Endpoint = EndpointForPrivateFolder;
        if(PublicList){
            Endpoint = EndpointForPublicFolder;
        }
        String URL = this.CRServer.getURL() + Endpoint;
        String AuthToken = this.CRServer.getAuthenticationToken();
        //System.out.println("Token:" + AuthToken);
        System.out.println(URL);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.get(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .asString();
        String JsonResponse = response.getBody();
       // System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();

        ArrayList<HashMap<String,Value>> AllFolderInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray FolderList = (JSONArray) jobj.get("list");
            for(int j=0;j<FolderList.size();j++){
                HashMap<String,Value> FolderInfoHash = new HashMap<String,Value>();

                JSONObject aFolder = (JSONObject) FolderList.get(j);
                String FolderID = (String) aFolder.get("id");
                String FolderparentID = (String) aFolder.get("parentId");
                String FolderName = (String) aFolder.get("name");
                String FolderPath = (String) aFolder.get("path");
                FolderInfoHash.put("id",new StringValue(FolderID));
                FolderInfoHash.put("parentid",new StringValue(FolderparentID));
                FolderInfoHash.put("name",new StringValue(FolderName));
                FolderInfoHash.put("path",new StringValue(FolderPath));

                AllFolderInfo.add(FolderInfoHash);

            }

            return AllFolderInfo;
        } catch (ParseException e) {
            return null;
        }
    }

    public String GetUseIdFromName(String UserName) throws UnirestException {
        ArrayList<HashMap<String,Value>> Results = GetUserListByName(UserName);
        return Results.get(0).get("id").get().toString();
    }

    public ArrayList<HashMap<String,Value>>  GetUserListByName(String UserName) throws UnirestException {
        String URL = this.CRServer.getURL() + "/v1/usermanagement/users/list";
        String AuthToken = this.CRServer.getAuthenticationToken();
        String BODY = JsonUtils.GetSearchBody(UserName,"username");
        System.out.println("Body:" + BODY);

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body(BODY)
                .asString();
        String JsonResponse = response.getBody();
        //System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        ArrayList<HashMap<String,Value>> AllObjectInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray ObjectList = (JSONArray) jobj.get("list");
            for(int j=0;j<ObjectList.size();j++){
                HashMap<String,Value> FolderInfoHash = new HashMap<String,Value>();

                JSONObject aaUser = (JSONObject) ObjectList.get(j);
                String ObjectID = aaUser.get("id").toString();
                String Username = (String) aaUser.get("username");
                String Domain = (String) aaUser.get("domain");
                FolderInfoHash.put("id",new StringValue(ObjectID));
                FolderInfoHash.put("username",new StringValue(Username));
                FolderInfoHash.put("domain",new StringValue(Domain));

                AllObjectInfo.add(FolderInfoHash);

            }

            return AllObjectInfo;
        } catch (ParseException e) {
            return null;
        }
    }

    public String GetRootFolderID(Boolean GetPublicFolderID) throws UnirestException {
        ArrayList<HashMap<String,Value>> AllFolders = GetFolderList(GetPublicFolderID);
        String ID = AllFolders.get(0).get("id").get().toString();
        //System.out.println("DEBUG Folder ID:"+ID);
        return ID;
    }

    public ArrayList<HashMap<String,Value>> GetBotListByName(Boolean SearchInPublicFolder, String Botname) throws UnirestException {
        String FolderID = GetRootFolderID(SearchInPublicFolder);
        String URL = this.CRServer.getURL() + "/v2/repository/folders/"+FolderID+"/list";
        String AuthToken = this.CRServer.getAuthenticationToken();
       // System.out.println("Token:" + AuthToken);
        //System.out.println(URL);
        String BODY = JsonUtils.GetSearchBody(Botname,"name");
        //System.out.println("DEBUG Bot Search:"+BODY);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body(BODY)
                .asString();
        String JsonResponse = response.getBody();
       //System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        ArrayList<HashMap<String,Value>> AllObjectInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray FolderList = (JSONArray) jobj.get("list");
            for(int j=0;j<FolderList.size();j++){
                HashMap<String,Value> FolderInfoHash = new HashMap<String,Value>();

                JSONObject aFolder = (JSONObject) FolderList.get(j);
                String ObjectID = (String) aFolder.get("id");
                String parentID = (String) aFolder.get("parentId");
                String ObjectName = (String) aFolder.get("name");
                String ObjectPath = (String) aFolder.get("path");
                FolderInfoHash.put("id",new StringValue(ObjectID));
                FolderInfoHash.put("parentid",new StringValue(parentID));
                FolderInfoHash.put("name",new StringValue(ObjectName));
                FolderInfoHash.put("path",new StringValue(ObjectPath));

                AllObjectInfo.add(FolderInfoHash);

            }

            return AllObjectInfo;
        } catch (ParseException e) {
            return null;
        }

    }


    public String GetBotIdFromName(Boolean SearchInPublicFolder, String Botname) throws UnirestException {
        ArrayList<HashMap<String,Value>> TheList = GetBotListByName(SearchInPublicFolder,Botname);
        String ID = TheList.get(0).get("id").get().toString();
        //System.out.println("DEBUG Bot ID:"+ID);
        return ID;
    }

    public String RunBot(String JSONBODY) throws UnirestException, ParseException {
        String URL = this.CRServer.getURL() + "/v3/automations/deploy";
        String AuthToken = this.CRServer.getAuthenticationToken();
        //System.out.println("Token:" + AuthToken);
        //System.out.println(URL);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                //.header("Content-Type", "text/plain")
                .body(JSONBODY)
                .asString();
        String JsonResponse = response.getBody();
        //System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();

        JSONObject jobj = (JSONObject) parse.parse(JsonResponse);

        this.CRServer.setAuthenticationToken(AuthToken);
        //System.out.println("Debug: " + AuthToken);
        return (String) jobj.get("deploymentId");

    }

    public Boolean AuthenticateToControlRoom() throws UnirestException {
        String LOGIN = this.CRServer.getLogin().getInsecureString();
        String PASSWORD = this.CRServer.getPwd().getInsecureString();
        String URL = this.CRServer.getURL();

        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL + "/v1/authentication")
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body("{\n    \"username\": \"" + LOGIN + "\",\n    \"password\": \"" + PASSWORD + "\",\n    \"captcha\": {}\n}")
                .asString();

        String JsonResponse = response.getBody();
        //System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            String AuthToken = (String) jobj.get("token");
            if (AuthToken == null) {
                return false;
            }
            this.CRServer.setAuthenticationToken(AuthToken);
            System.out.println("Debug: " + AuthToken);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public ArrayList<HashMap<String,Value>> GetDevicePoolList(String DevicePoolName) throws UnirestException {
        String URL = this.CRServer.getURL() + "/v2/devices/pools/list";
        String AuthToken = this.CRServer.getAuthenticationToken();
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .body(JsonUtils.GetSearchBody(DevicePoolName,"name"))
                .asString();
        String JsonResponse = response.getBody();
        System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        ArrayList<HashMap<String,Value>> AllObjectInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray FolderList = (JSONArray) jobj.get("list");
            for(int j=0;j<FolderList.size();j++){
                HashMap<String,Value> FolderInfoHash = new HashMap<String,Value>();

                /*

      "id": "4",
      "name": "MyPool1",
      "status": "CONNECTED",
      "detailedStatus": "ALL_CONNECTED",
      "automationCount": "0",
      "ownerIds": [
        "3"
      ],
      "deviceCount": "1"
                 */

                JSONObject anObject = (JSONObject) FolderList.get(j);
                String ObjectID = (String) anObject.get("id");
                String ObjectName = (String) anObject.get("name");
                String ObjectStatus = (String) anObject.get("status");
                String ObjectDetailedStatus = (String) anObject.get("detailedStatus");
                String ObjectCount = (String) anObject.get("automationCount");
                String ObjectDeviceCount = (String) anObject.get("deviceCount");

                JSONArray ListOfOwners = (JSONArray) anObject.get("ownerIds");
                String AllOwners = "";
                for(int k=0;k< ListOfOwners.size();k++){
                    String owner = (String) ListOfOwners.get(k);
                    if(AllOwners.equalsIgnoreCase("")){
                        AllOwners = owner;
                    }else{
                        AllOwners=AllOwners+",";
                    }
                }


                FolderInfoHash.put("id",new StringValue(ObjectID));
                FolderInfoHash.put("name",new StringValue(ObjectName));
                FolderInfoHash.put("status",new StringValue(ObjectStatus));
                FolderInfoHash.put("detailedstatus",new StringValue(ObjectDetailedStatus));
                FolderInfoHash.put("automationcount",new StringValue(ObjectCount));
                FolderInfoHash.put("devicecount",new StringValue(ObjectDeviceCount));
                FolderInfoHash.put("owners",new StringValue(AllOwners));


                AllObjectInfo.add(FolderInfoHash);

            }

            return AllObjectInfo;
        } catch (ParseException e) {
            return null;
        }

    }

    public String GetDevicePoolIdFromName(String DevicePoolName) throws UnirestException {
        ArrayList<HashMap<String,Value>> allDP = GetDevicePoolList(DevicePoolName);
        String ID = allDP.get(0).get("id").get().toString();
        //System.out.println("DEBUG Pool ID:"+ID);

        return ID;
    }

    public ArrayList<HashMap<String,Value>> GetDeviceList() throws UnirestException {

        String URL = this.CRServer.getURL() + "/v2/devices/list";
        String AuthToken = this.CRServer.getAuthenticationToken();
        System.out.println("Token:" + AuthToken);
        System.out.println(URL);
        Unirest.setTimeouts(0, 0);
        HttpResponse<String> response = Unirest.post(URL)
                .header("X-Authorization", AuthToken)
                .header("Content-Type", "application/json")
                .header("Content-Type", "text/plain")
                .body("{\n    \"sort\": [\n        {\n            \"field\": \"updatedOn\",\n            \"direction\": \"desc\"\n        }\n    ],\n    \"filter\": {},\n    \"fields\": [],\n    \"page\": {\n        \"offset\": 0,\n        \"total\": 1,\n        \"totalFilter\": 1,\n        \"length\": 100\n    }\n}")
                .asString();
        String JsonResponse = response.getBody();
        //System.out.println("DEBUG:" + JsonResponse);
        JSONParser parse = new JSONParser();
        ArrayList<HashMap<String,Value>> AllObjectInfo = new ArrayList<HashMap<String,Value>>();

        try {
            JSONObject jobj = (JSONObject) parse.parse(JsonResponse);
            JSONArray FolderList = (JSONArray) jobj.get("list");
            for(int j=0;j<FolderList.size();j++){
                HashMap<String,Value> FolderInfoHash = new HashMap<String,Value>();

                JSONObject anObject = (JSONObject) FolderList.get(j);
                String ObjectID = (String) anObject.get("id");
                String ObjectType = (String) anObject.get("type");
                String ObjectHostName = (String) anObject.get("hostName");
                String ObjectUserId = (String) anObject.get("userId");
                String ObjectUserName = (String) anObject.get("userName");
                String ObjectStatus = (String) anObject.get("status");
                String ObjectPoolName = (String) anObject.get("poolName");
                String ObjectBotAgentVersion = (String) anObject.get("botAgentVersion");

                FolderInfoHash.put("id",new StringValue(ObjectID));
                FolderInfoHash.put("type",new StringValue(ObjectType));
                FolderInfoHash.put("hostname",new StringValue(ObjectHostName));
                FolderInfoHash.put("userid",new StringValue(ObjectUserId));
                FolderInfoHash.put("username",new StringValue(ObjectUserName));
                FolderInfoHash.put("status",new StringValue(ObjectStatus));
                FolderInfoHash.put("poolname",new StringValue(ObjectPoolName));
                FolderInfoHash.put("botagentversion",new StringValue(ObjectBotAgentVersion));

                AllObjectInfo.add(FolderInfoHash);

            }

            return AllObjectInfo;
        } catch (ParseException e) {
            return null;
        }

    }

}
