package Utils;

import com.automationanywhere.botcommand.data.Value;
import org.json.JSONObject;
import org.json.simple.JSONArray;

import javax.security.auth.callback.Callback;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class JsonUtils {

    public static String GetAddWorkItemBody(Map<String,Value> Values){
        /*
        {
  "workItems": [
    {
      "json": {
           "firstname": "Yli",
    	"lastname": "Z",
    	"dob": "1111111",
    	"membershipnumber": ""
      }
    }
  ]
}
         */

        // Some stuff in the request is hardcoded.. this could easily be changed.
        JSONArray ItemsArray = new JSONArray();

        JSONObject AllItems = new JSONObject();
        for (Map.Entry<String,Value> entry : Values.entrySet()){
            AllItems.put(entry.getKey(),entry.getValue());
        }
        ItemsArray.add(new JSONObject().put("json",AllItems));
        JSONObject JSONBODY = new JSONObject().put("workItems", ItemsArray);

        //System.out.println(JSONBODY.toString());
        return JSONBODY.toString();
    }

    /*
    {"filter":{"operator":"substring","value":"botrunner1","field":"username"},"fields":[],"page":{"offset":0,"total":16,"totalFilter":16,"length":100}}
     */
    public static String GetSearchBody(String NameFilter, String FieldNameForFilter){
/**
        {
            "filter": {
            "operator": "eq",
            "value": "API Bot",
            "field": "name"
        },

            "page":{
            "offset":0,
            "length":10
        }
        }
     **/

        JSONObject filter = new JSONObject()
                .put("operator","eq")
                .put("value",NameFilter)
                .put("field",FieldNameForFilter);

        JSONObject page = new JSONObject()
                .put("offset",0)
                .put("length",10);

        // Some stuff in the request is hardcoded.. this could easily be changed.
        JSONObject JSONBODY = new JSONObject()
                .put("filter", filter)
                .put("page", page);

        System.out.println(JSONBODY.toString());
        return JSONBODY.toString();

    }

    /*
    {
    "name": "API Bot.20.04.10.23.07.07.iqbot1",
    "automationName": "API Bot.20.04.10.23.07.07.iqbot1",
    "fileId": "433",
    "fileName": "API Bot",
    "status": "ACTIVE",
    "rdpEnabled": false,
    "runElevated": false,
    "setAsDefaultDevice": false,
    "scheduleType": "INSTANT"
    "workspaceName": "public",
    "timeZone": "Africa/Abidjan",
    "botInput": {
                "sInputString1": {
            "type": "STRING",
            "string": "rgdfg"
        },
        "iInputNumber": {
            "type": "NUMBER",
            "number": "324"
        },
        "bBooleanInput": {
            "type": "BOOLEAN",
            "boolean": true
        },
        "dInputDict1": {
            "type": "DICTIONARY",
            "dictionary": [
                {
                    "key": "Key 1",
                    "value": {
                        "type": "STRING",
                        "string": "cvbdfgdfg"
                    }
                }
            ]
        }

    },
    "runAsUserIds": [
        "95"
    ],
     "poolIds": [
        "4"
    ],
    "callbackInfo": {"url": "http://localhost:4100/getoutput"
}
     */

    public static String GetRunBotBody(String UniqueID, String BotID, String BotName, ArrayList<String> UserIDList, ArrayList<String> PoolIDList, String CallBackURL, LinkedHashMap<String, Value> InputParams){

        JSONArray UserIDs = new JSONArray();
        for(String userID : UserIDList){ UserIDs.add(userID);}

        JSONArray PoolIDs = new JSONArray();
        for(String PoolID : PoolIDList){ PoolIDs.add(PoolID);}

        // Some stuff in the request is hardcoded.. this could easily be changed.
        JSONObject JSONBODY = new JSONObject()
                .put("name", UniqueID)
                .put("automationName", UniqueID)
                .put("fileId", BotID)
                .put("status", "ACTIVE")
                .put("rdpEnabled", false)
                .put("runElevated", false)
                .put("workspaceName", "public")
                .put("timeZone", "Africa/Abidjan")
                .put("scheduleType", "INSTANT")
                .put("botInput",new JSONObject())
                .put("runAsUserIds",UserIDs)
                .put("poolIds", PoolIDs);

        // Only adding the Callback URL if it is requested in the action
        if(CallBackURL != null && !CallBackURL.equals("")){
            JSONBODY.put("callbackInfo", new JSONObject().put("url",CallBackURL));
        }

        if(InputParams!= null && !InputParams.isEmpty()){
            // TO DO: add implementation to pass input parameters to Bot
        }

        //System.out.println(JSONBODY.toString());
        return JSONBODY.toString();
    }
}
